# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This playbook is intended for development use only: Neither importing nor
# including the role in other playbooks will cause evaluation of this file.
---

# https://docs.ansible.com/ansible/latest/user_guide/playbooks.html
- name:
    "provision-icinga-ssh-agents"

  hosts:
    "all"

  module_defaults:

    # https://docs.ansible.com/ansible/latest/modules/apt_module.html
    apt:
      cache_valid_time: 600
      update_cache: true

  tasks:

    # https://docs.ansible.com/ansible/latest/import_role_module.html
    - import_role:
        name: "icinga/ssh-agent"
      vars:
        icinga_host_options:
          enable_flapping: "true"

    # https://docs.ansible.com/ansible/latest/import_role_module.html
    - import_role:
        name: ".examples/monitoring-plugins"
      vars:
        monitoring_plugins: "{{ icinga_ssh_agent_monitoring_plugins }}"

    # https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
    - name:
        "generate_service_definitions"
      include_tasks:
        "server/tasks/generate_service_definitions.yml"
      loop:
        "{{ icinga_ssh_agent_server_associations }}"
      loop_control:
        label: "{{ item.name }}"
      vars:
        custom_icinga_services: "{{ icinga_ssh_agent_icinga_services }}"
        # yamllint disable rule:line-length
        common_service_header: |
          # generated by https://gitlab.com/eyeo/devops/ansible-role-icinga/provision-icinga-ssh-agents.yml
        # yamllint enable rule:line-length
        icinga_server_name: "{{ item.name }}"
        icinga_server_base_dir: "server"

  vars:
    icinga_notification_handler:
      "server : service : icinga2"
