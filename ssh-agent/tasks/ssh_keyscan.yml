# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# Detect all public SSH keys of the Icinga agent host.
#
# This could be done directly on the agent itself, but delegating to a server
# ensures the route working and avoids potential issues when the agent has
# a firewall where itself is not included in the list of source IPs allowed to
# connect via SSH.
#
# https://docs.ansible.com/ansible/latest/command_module.html
# https://www.linux.org/docs/man1/ssh-keyscan.html
- name:
    "server : command : ssh-keyscan"
  command:
    "ssh-keyscan -f -"
  args:
    stdin: "{{ icinga_ssh_agent_ip_addresses }} {{ icinga_ssh_agent_names }}"
  vars:
    icinga_ssh_agent_ip_addresses:
      "{{ [icinga_ssh_agent_ipv6_address, icinga_ssh_agent_ipv4_address]
        | difference([omit, none, '']) | join(',') }}"
    icinga_ssh_agent_names:
      "{{ ansible_fqdn }},{{ icinga_ssh_agent_ip_addresses }}"
  changed_when:
    false
  register:
    "icinga_ssh_agent_keyscan"
  delegate_to:
    "{{ icinga_ssh_agent_server_association_name }}"

# https://docs.ansible.com/ansible/latest/set_fact_module.html
- name:
    "set_fact : icinga_ssh_agent_public_keys"
  set_fact:
    icinga_ssh_agent_public_keys:
      "{{ icinga_ssh_agent_keyscan.stdout_lines }}"
