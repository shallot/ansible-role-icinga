# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/include_vars_module.html
- include_vars:
    "vars/defaults.yml"

- include_vars:
    "{{ icinga_server_vars }}"
  with_first_found:
    # yamllint disable rule:line-length
    - "vars/{{ ansible_os_family | lower }}/{{ ansible_distribution_release }}.yml"
    - "vars/{{ ansible_os_family | lower }}.yml"
    # yamllint enable rule:line-length
  loop_control:
    loop_var: "icinga_server_vars"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "{{ icinga_server_tasks }}"
  with_first_found:
    - "os/{{ ansible_os_family | lower }}/main.yml"
    - "os/generic/main.yml"
  loop_control:
    loop_var: "icinga_server_tasks"

# https://docs.ansible.com/ansible/latest/template_module.html
- name:
    "file : ido-pgsql.conf"
  template:
    dest: "/etc/icinga2/features-available/ido-pgsql.conf"
    group: "root"
    mode: "0644"
    owner: "root"
    src: "ido-pgsql.conf.j2"
  notify:
    - "service : icinga2"
  become:
    true

# https://docs.ansible.com/ansible/latest/icinga2_feature_module.html
- name:
    "features"
  icinga2_feature:
    name: "{{ item.key }}"
    state: "{{ item.value }}"
  loop:
    "{{ icinga_server_features
       | combine({'ido-pgsql': 'present'})
       | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value] | to_json }}"
  notify:
    - "service : icinga2"
  become:
    true

# https://docs.ansible.com/ansible/latest/user_module.html
- user:
    generate_ssh_key: true
    name: "{{ icinga_server_system_user }}"
    shell: "{{ icinga_server_system_shell }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/shell_module.html
- name:
    "shell : mv conf.d/... /root/backup-icinga2-..."
  shell: |
    if [ -e {{ item | quote }} ]; then
      mv -v {{ item | quote }} /root/backup-icinga2-{{ item | quote }}
    fi
  args:
    chdir: "{{ icinga_server_conf_directory }}"
  register:
    "icinga_server_configuration_backup"
  changed_when:
    "icinga_server_configuration_backup.stdout_lines"
  loop:
    "{{ icinga_server_obsolete_configuration_files }}"
  notify:
    - "service : icinga2"
  become:
    true

# https://docs.ansible.com/ansible/latest/file_module.html
- name:
    "directory : conf.d/*"
  file:
    group: "{{ icinga_server_conf_directory_group }}"
    mode: "0755"
    owner: "{{ icinga_server_conf_directory_user }}"
    path: "{{ icinga_server_conf_directory }}"
    state: "directory"
  become:
    true

# https://docs.ansible.com/ansible/latest/file_module.html
- name:
    "directories : conf.d/*"
  file:
    group: "root"
    mode: "0755"
    owner: "root"
    path: "{{ icinga_server_conf_directory }}/{{ item }}"
    state: "directory"
  loop:
    - "apps"
    - "commands"
    - "groups/hosts"
    - "groups/services"
    - "groups/users"
    - "hosts"
    - "services"
    - "users"
    - "notifications"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- name:
    "generate_command_definitions"
  include_tasks:
    "generate_command_definitions.yml"
  vars:
    custom_icinga_commands:
      "{{ icinga_server_commands }}"
    icinga_notification_handler:
      "service : icinga2"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- name:
    "generate_usergroup_definitions"
  include_tasks:
    "generate_usergroup_definitions.yml"
  vars:
    custom_icinga_user_groups:
      "{{ icinga_server_user_groups }}"
    custom_icinga_service_directory:
      "{{ icinga_server_conf_directory }}/groups/users"
    icinga_notification_handler:
      "service : icinga2"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- name:
    "generate_user_definitions"
  include_tasks:
    "generate_user_definitions.yml"
  vars:
    custom_icinga_users:
      "{{ icinga_server_users }}"
    custom_icinga_service_directory:
      "{{ icinga_server_conf_directory }}/users"
    icinga_notification_handler:
      "service : icinga2"

# https://docs.ansible.com/ansible/latest/copy_module.html
- name:
    "files : /etc/icinga2/conf.d/groups/hosts/*.conf"
  copy:
    content: "{{ item.content | default(omit) }}"
    dest:
      "{{ icinga_server_conf_directory }}/groups/hosts/{{ item.name }}.conf"
    group: "root"
    mode: "0644"
    owner: "root"
    remote_src: "{{ item.remote_src | default(omit) }}"
    src: "{{ item.src | default(omit) }}"
  loop:
    "{{ icinga_server_host_groups | flatten(levels=1) }}"
  loop_control:
    label: "{{ item.name }}"
  notify:
    - "service : icinga2"
  become:
    true

# https://docs.ansible.com/ansible/latest/copy_module.html
- name:
    "files : /etc/icinga2/conf.d/groups/services/*.conf"
  copy:
    content: "{{ item.content | default(omit) }}"
    dest:
      "{{ icinga_server_conf_directory }}/groups/services/{{ item.name }}.conf"
    group: "root"
    mode: "0644"
    owner: "root"
    remote_src: "{{ item.remote_src | default(omit) }}"
    src: "{{ item.src | default(omit) }}"
  loop:
    "{{ icinga_server_service_groups | flatten(levels=1) }}"
  loop_control:
    label: "{{ item.name }}"
  notify:
    - "service : icinga2"
  become:
    true
