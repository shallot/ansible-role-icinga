# Copyright (c) 2018-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/modules/include_vars_module.html
- include_vars:
    "{{ icinga_web_vars }}"
  with_first_found:
    # yamllint disable rule:line-length
    - "vars/{{ ansible_os_family | lower }}/{{ ansible_distribution_release | lower }}.yml"
    # yamllint enable rule:line-length
    - "vars/{{ ansible_os_family | lower }}.yml"
    - "vars/debian.yml"
  loop_control:
    loop_var: "icinga_web_vars"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "{{ icinga_web_server_tasks }}"
  with_first_found:
    - "os/{{ ansible_os_family | lower }}/main.yml"
    - "os/generic/main.yml"
  loop_control:
    loop_var: "icinga_web_server_tasks"

# https://docs.ansible.com/ansible/latest/copy_module.html
- name:
    "file : timezone.ini"
  copy:
    content:
      "data.timezone = {{ icinga_web_php_timezone }}"
    dest:
      "{{ icinga_web_php_configuration_directory }}/timezone.ini"
    owner: "root"
    group: "root"
    mode: "0644"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/user_module.html
- name:
    "user : www-data"
  user:
    name: "{{ icinga_web_httpd_user }}"
    state: "present"
    groups:
      ["icingaweb2",
       "{{ icinga_web_httpd_user }}"]
    append: true
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/file_module.html
- name:
    "directory : /etc/icingaweb2/modules"
  file:
    path: "{{ item }}"
    state: "directory"
    owner: "root"
    group: "icingaweb2"
    mode: "0775"
  loop_control:
    label: "{{ item | to_json }}"
  loop:
    - "/etc/icingaweb2"
    - "/etc/icingaweb2/modules"
  become:
    true

# https://docs.ansible.com/ansible/latest/ini_file_module.html
- name:
    "ini_file : authentication.ini"
  ini_file:
    dest: "/etc/icingaweb2/authentication.ini"
    section: "{{ (item.key | splitext)[0] }}"
    option: "{{ (item.key | splitext)[1][1:] }}"
    value: "{{ item.value }}"
    owner: "root"
    group: "icingaweb2"
    mode: "0644"
  loop:
    "{{ icinga_web_auth_settings | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value] | to_json }}"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/ini_file_module.html
- name:
    "ini_file : config.ini"
  ini_file:
    dest: "/etc/icingaweb2/config.ini"
    section: "{{ (item.key | splitext)[0] }}"
    option: "{{ (item.key | splitext)[1][1:] }}"
    value: "{{ item.value }}"
    owner: "root"
    group: "icingaweb2"
    mode: "0644"
  loop:
    "{{ icinga_web_config_settings | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value] | to_json }}"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/ini_file_module.html
- name:
    "ini_file : resources.ini"
  ini_file:
    dest: "/etc/icingaweb2/resources.ini"
    section: "{{ (item.key | splitext)[0] }}"
    option: "{{ (item.key | splitext)[1][1:] }}"
    value: "{{ item.value }}"
    owner: "root"
    group: "icingaweb2"
    mode: "0644"
  loop:
    "{{ icinga_web_resources_settings | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value] | to_json }}"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/ini_file_module.html
- name:
    "ini_file : roles.ini"
  ini_file:
    dest: "/etc/icingaweb2/roles.ini"
    section: "{{ section_option_split[0] }}"
    option: "{{ section_option_split[1] }}"
    value: "{{ item.value }}"
    owner: "root"
    group: "icingaweb2"
    mode: "0644"
  vars:
    section_option_param: "{{ item.value.name | default(item.key) }}"
    section_option_split: "{{ section_option_param.split('.') }}"
  loop:
    "{{ icinga_web_roles_settings | dict2items }}"
  loop_control:
    label: "{{ [item.key, item.value, section_option_split] | to_json }}"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/shell_module.html
- name:
    "shell : icingaweb tables check"
  shell: >-
    psql
    -U postgres
    -d {{ icinga_web_database_name }}
    -c '\dt'
    | grep icingaweb_user_preference
    || true
  register:
    "icingaweb_tables_exist"
  become_user:
    "postgres"
  changed_when:
    false
  become:
    true

# https://docs.ansible.com/ansible/latest/postgresql_db_module.html
- name:
    "postgresql_db : import icingaweb tables"
  postgresql_db:
    name: "{{ icinga_web_database_name }}"
    state: "restore"
    target: "{{ icinga_web_tables_location }}"
  register:
    "icingaweb_tables_import"
  when:
    "'icingaweb_user_preference' not in icingaweb_tables_exist.stdout"
  become_user:
    "postgres"
  become:
    true

# https://docs.ansible.com/ansible/latest/shell_module.html
- name:
    "shell : users"
  shell: >-
    psql
    -d "{{ icinga_web_database_name }}"
    -c "INSERT INTO icingaweb_user (name, active, password_hash)
        VALUES (
          '{{ item.key }}',
          '{{ icinga_web_user_active }}',
          '{{ icinga_web_user_password_hash }}')
        ON CONFLICT (name) DO UPDATE
        SET active='{{ icinga_web_user_active }}',
          password_hash='{{ icinga_web_user_password_hash }}'
        WHERE
          icingaweb_user.active IS DISTINCT FROM '{{ icinga_web_user_active }}'
          OR icingaweb_user.password_hash IS DISTINCT FROM
          '{{ icinga_web_user_password_hash }}'"
  register:
    "insert_result"
  loop:
    "{{ icinga_web_users | dict2items }}"
  vars:
    icinga_web_user_active:
      "{{ item.value.active | default(true) | ternary(1, 0) }}"
    icinga_web_user_password_hash:
      "{{ item.value.password_hash | default('') }}"
  loop_control:
    label: "{{ [item.key, {'active': icinga_web_user_active}] | to_json }}"
  changed_when:
    "'1' in insert_result.stdout"
  become_user:
    "postgres"
  notify:
    "{{ icinga_web_httpd_handlers or omit }}"
  become:
    true

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "monitoring.yml"
